# README #

An example repository for a sphinx documentation project using read-the-docs theme.
Included is a docker-based build tool.  
Some custom css added, volume mounts defined so the changes work when running make clean.  

Sphinx docs: https://www.sphinx-doc.org/en/master/usage/index.html  
RTD docs: https://docs.readthedocs.io/en/latest/index.html   

## Usage ##
---
The docs folder contains the source files in the structure defined by sphinx.

```
Note: The build directory is ignored in git, so the html files need to be built first.
```

Development environment is started using docker-compose inside the docker directory:

* ```docker-compose up -d```
* ```docker-compose exec doc bash```
* ```make html```


Or do everything using the dev script (read the file named 'dev' to see the commands)

* ```bash dev {Command}```